/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Stephen Foiles (SNL), Murray Daw (SNL)
------------------------------------------------------------------------- */

#include "pair_eam.h"
#include <mpi.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include "atom.h"
#include "force.h"
#include "comm.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "memory.h"
#include "error.h"

using namespace LAMMPS_NS;

#define MAXLINE 1024

/* ---------------------------------------------------------------------- */

PairEAM::PairEAM(LAMMPS *lmp) : Pair(lmp)
{
#ifdef RAJATRACE
  printf("PairEAM::PairEAM\n");
#endif
  restartinfo = 0;
  manybody_flag = 1;

  nmax = 0;
  rho = NULL;
  fp = NULL;
  map = NULL;
  type2frho = NULL;

  nfuncfl = 0;
  funcfl = NULL;

  setfl = NULL;
  fs = NULL;

  frho = NULL;
  rhor = NULL;
  z2r = NULL;
  scale = NULL;

  frho_spline = NULL;
  rhor_spline = NULL;
  z2r_spline = NULL;

  // set comm size needed by this Pair

  comm_forward = 1;
  comm_reverse = 1;
  
#ifdef RAJACUDA
  cudaGetDeviceCount(&nGPUs);
  myGPU = comm->me%nGPUs;
  gec(cudaSetDevice(myGPU));
#endif

  raja_allocated = false;
  h_rho = NULL;
  h_fp = NULL;
  h_eatom = NULL;
  u_scale = NULL;
  h_x = NULL;
  h_f = NULL;
  
  u_type2frho = NULL;
  u_type2rhor = NULL;
  u_type2z2r = NULL;
  h_firstneigh = NULL;
}

/* ----------------------------------------------------------------------
   check if allocated, since class can be destructed when incomplete
------------------------------------------------------------------------- */

PairEAM::~PairEAM()
{
#ifdef RAJATRACE
  printf("PairEAM::~PairEAM\n");
#endif
  if (copymode) return;

  memory->destroy(rho);
  memory->destroy(fp);

  if (allocated) {
    memory->destroy(setflag);
    memory->destroy(cutsq);
    delete [] map;
    delete [] type2frho;
    map = NULL;
    type2frho = NULL;
    memory->destroy(type2rhor);
    memory->destroy(type2z2r);
    memory->destroy(scale);
  }

  if (funcfl) {
    for (int i = 0; i < nfuncfl; i++) {
      delete [] funcfl[i].file;
      memory->destroy(funcfl[i].frho);
      memory->destroy(funcfl[i].rhor);
      memory->destroy(funcfl[i].zr);
    }
    memory->sfree(funcfl);
    funcfl = NULL;
  }

  if (setfl) {
    for (int i = 0; i < setfl->nelements; i++) delete [] setfl->elements[i];
    delete [] setfl->elements;
    delete [] setfl->mass;
    memory->destroy(setfl->frho);
    memory->destroy(setfl->rhor);
    memory->destroy(setfl->z2r);
    delete setfl;
    setfl = NULL;
  }

  if (fs) {
    for (int i = 0; i < fs->nelements; i++) delete [] fs->elements[i];
    delete [] fs->elements;
    delete [] fs->mass;
    memory->destroy(fs->frho);
    memory->destroy(fs->rhor);
    memory->destroy(fs->z2r);
    delete fs;
    fs = NULL;
  }

  memory->destroy(frho);
  memory->destroy(rhor);
  memory->destroy(z2r);

  memory->destroy(frho_spline);
  memory->destroy(rhor_spline);
  memory->destroy(z2r_spline);
  
  #ifdef RAJACUDA

  if (raja_allocated)
  {
    gec(cudaFree(u_frho_spline));
    gec(cudaFree(u_rhor_spline));
    gec(cudaFree(u_z2r_spline));  
    gec(cudaFree(u_scale));  
    
    memory->destroy(h_rho);
    memory->destroy(h_fp);
    memory->destroy(h_x);
    memory->destroy(h_f);

    raja_allocated = false;
  }
  
  gec(cudaDeviceReset());
  #endif
}

/* ---------------------------------------------------------------------- */

void PairEAM::compute(int eflag, int vflag)
{
#ifdef RAJATRACE
  printf("PairEAM::compute\n");
#endif

  int inum;
  int *ilist,*numneigh,**firstneigh;

  ev_init(eflag,vflag);
  
  if (atom->nmax > nmax) {
    nmax = atom->nmax;
  }
  
  double **x = atom->x;
  double **f = atom->f;
  int *type = atom->type;
  int nlocal = atom->nlocal;
  int newton_pair = force->newton_pair;

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;     // # of J neighbors for each I atom
  firstneigh = list->firstneigh; // ptr to 1st J int value of each I atom
  
  real_t 
         *d_rho          = NULL,
         *d_fp           = NULL,
         *d_eatom        = NULL,
         *d_x            = NULL,
         *d_f            = NULL,
         f_rhomax        = static_cast<real_t> (rhomax),
         f_rdrho         = static_cast<real_t> (rdrho),
         f_cutforcesq    = static_cast<real_t> (cutforcesq),
         f_rdr           = static_cast<real_t> (rdr),
         &d_rdr          = f_rdr,
         &d_rhomax       = f_rhomax,
         &d_rdrho        = f_rdrho,
         &d_cutforcesq   = f_cutforcesq
         ;

  int    
         Xfrho           = nfrho, 
         Xrhor           = nrhor, 
         Xz2r            = nz2r, 
         Yfrho           = nrho+1,
         Yrhor           = nr+1,
         Yz2r            = nr+1,
         Z               = 7,
         xfdim           = nmax, //tmax, //nall,
         *d_type         = NULL, 
         *d_ilist        = NULL,
         *d_firstneigh   = NULL, 
         *d_numneigh     = NULL,
         &d_nr           = nr,
         &d_nrho         = nrho,
         &d_eflag        = eflag,
         &d_eflag_atom   = eflag_atom,
         &d_eflag_global = eflag_global,
         &d_newton_pair  = newton_pair,
         &d_nlocal       = nlocal
         
         ;
  nt2fr   = atom->ntypes + 1;       
  maxjnum = 0;
  for (int ii = 0; ii < inum; ii++)
  {
    int i = ilist[ii];
    int jnum = numneigh[i];
    if (maxjnum < jnum) maxjnum = jnum;
  }

  // grow energy and fp arrays if necessary
  // need to be atom->nmax in length

  if (raja_allocated)
  {
    memory->destroy(h_rho);
    memory->destroy(h_fp);
    memory->destroy(h_x);
    memory->destroy(h_f);
    raja_allocated = false;
  }
  if (!raja_allocated)
  {
    memory->create(h_rho, nmax, "pair:h_rho");
    memory->create(h_fp, nmax, "pair:h_fp");
    memory->create(h_x, xfdim*3, "pair:h_x");
    memory->create(h_f, xfdim*3, "pair:h_f");
    
    memory->create(h_firstneigh, inum * maxjnum, "neighbor:h_neighlist");
    memset(h_firstneigh, -1, inum*maxjnum*sizeof(int));
  
    gec(cudaMalloc((void **) &d_x, xfdim*3*sizeof(real_t)));
    gec(cudaMalloc((void **) &d_f, xfdim*3*sizeof(real_t)));
    
    gec(cudaMalloc((void **) &d_rho, nmax * sizeof(real_t)));
    gec(cudaMalloc((void **) &d_fp , nmax * sizeof(real_t)));
    
    
    gec(cudaMalloc((void **) &d_ilist, inum * sizeof(int)));
    gec(cudaMalloc((void **) &d_type, xfdim * sizeof(int)));

    gec(cudaMalloc((void **) &d_numneigh, inum * sizeof(int)));
    gec(cudaMalloc((void **) &d_firstneigh, inum * maxjnum * sizeof(int)));

    gec(cudaMemset(d_rho, static_cast<real_t>(0.0), nmax * sizeof(real_t)));
    gec(cudaMemset(d_fp,  static_cast<real_t>(0.0), nmax * sizeof(real_t)));

    if (eflag_atom)
    {
      memory->create(h_eatom, xfdim, "pair:h_eatom");
      gec(cudaMalloc((void **) &d_eatom, xfdim * sizeof(real_t)));
    }
    raja_allocated = true;
  }
  gec(cudaMemset(d_firstneigh, -1, inum * maxjnum * sizeof(int)));
  
  gec(cudaMallocManaged((void **) &u_scale, nt2fr * nt2fr * sizeof(real_t)));
  cudaDeviceSynchronize();

  RAJA::View<real_t, RAJA::Layout<2>> h_x_view(h_x, xfdim, 3);
  RAJA::View<real_t, RAJA::Layout<2>> h_f_view(h_f, xfdim, 3);

  // zero out density
  for (int i = 0; i < nmax; i++)
  {
    h_x_view(i,0) = static_cast<real_t> (x[i][0]);
    h_x_view(i,1) = static_cast<real_t> (x[i][1]);
    h_x_view(i,2) = static_cast<real_t> (x[i][2]);

    h_f_view(i,0) = static_cast<real_t> (f[i][0]);
    h_f_view(i,1) = static_cast<real_t> (f[i][1]);
    h_f_view(i,2) = static_cast<real_t> (f[i][2]);
  }

  RAJA::View<int, RAJA::Layout<1>> u_type2frho_view(u_type2frho, nt2fr);
  RAJA::View<int, RAJA::Layout<2>> u_type2rhor_view(u_type2rhor, nt2fr, nt2fr);
  RAJA::View<int, RAJA::Layout<2>> u_type2z2r_view(u_type2z2r, nt2fr, nt2fr);
  RAJA::View<real_t, RAJA::Layout<2>> u_scale_view(u_scale, nt2fr, nt2fr);

  for (int i = 0; i < nt2fr; i++)
  {
    u_type2frho[i] = type2frho[i];
    for (int j = 0; j < nt2fr; j++)
    {
      u_type2rhor_view(i,j) = type2rhor[i][j];
      u_type2z2r_view(i,j) = type2z2r[i][j];
      u_scale_view(i,j) = scale[i][j];
    }
  }
  cudaDeviceSynchronize();
  
  RAJA::View<real_t, RAJA::Layout<3>> u_frho_spline_view(u_frho_spline, Xfrho, Yfrho, Z);
  RAJA::View<real_t, RAJA::Layout<3>> u_rhor_spline_view(u_rhor_spline, Xrhor, Yrhor, Z);
  RAJA::View<real_t, RAJA::Layout<3>> u_z2r_spline_view (u_z2r_spline , Xz2r , Yz2r , Z);
  
  gec(cudaMemcpy(    d_x,   h_x, xfdim *     3 * sizeof(real_t), cudaMemcpyHostToDevice));
  gec(cudaMemcpy(    d_f,   h_f, xfdim *     3 * sizeof(real_t), cudaMemcpyHostToDevice));
  gec(cudaMemcpy(d_ilist, ilist,          inum *    sizeof(int), cudaMemcpyHostToDevice));
  gec(cudaMemcpy( d_type,  type,         xfdim *    sizeof(int), cudaMemcpyHostToDevice));
  
  RAJA::View<real_t, RAJA::Layout<2>> d_x_view(d_x, xfdim, 3);
  RAJA::View<real_t, RAJA::Layout<2>> d_f_view(d_f, xfdim, 3);

  RAJA::View<int, RAJA::Layout<2>> h_firstneigh_view(h_firstneigh, inum, maxjnum);
  RAJA::View<int, RAJA::Layout<2>> d_firstneigh_view(d_firstneigh, inum, maxjnum);

  for (int ii = 0; ii < inum; ii++)
  {
    int i = ilist[ii];
    int jnum = numneigh[i];
    for (int jj = 0; jj < jnum; jj++)
    {
      int j = firstneigh[i][jj];
      h_firstneigh_view(i,jj) = j;
    }
  }
  gec(cudaMemcpy(d_numneigh,       numneigh, inum  *           sizeof(int), cudaMemcpyHostToDevice));  
  gec(cudaMemcpy(d_firstneigh, h_firstneigh, inum  * maxjnum * sizeof(int), cudaMemcpyHostToDevice));
  
  
  RAJA::forall<RAJA::cuda_exec<CUDA_BLOCK_SIZE>>(RAJA::RangeSegment(0, inum), [=] RAJA_DEVICE (int ii) 
  {
    int d_i = d_ilist[ii];
    real_t d_xtmp = d_x_view(d_i,0);
    real_t d_ytmp = d_x_view(d_i,1);
    real_t d_ztmp = d_x_view(d_i,2);
    int d_itype = d_type[d_i];
    int d_jnum = d_numneigh[d_i];
    for (int jj = 0; jj < d_jnum; jj++)
    {
      int d_j = d_firstneigh_view(d_i, jj);
      d_j &= NEIGHMASK;

      real_t d_delx = d_xtmp - d_x_view(d_j,0);
      real_t d_dely = d_ytmp - d_x_view(d_j,1);
      real_t d_delz = d_ztmp - d_x_view(d_j,2);
      real_t d_rsq  = d_delx*d_delx + d_dely*d_dely + d_delz*d_delz;
      
      if (d_rsq < d_cutforcesq)
      {
        real_t d_jtype = d_type[d_j];
        real_t d_p = sqrtr(d_rsq)*d_rdr + static_cast<real_t>(1.0);
        int d_m = static_cast<int> (d_p);
        d_m  = MIN(d_m, d_nr-1);
        d_p -= static_cast<real_t> (d_m);
        d_p  = MIN(d_p, static_cast<real_t> (1.0));
        int t2rhorji = u_type2rhor_view(d_jtype, d_itype);
        real_t d_coeff3ji = u_rhor_spline_view(t2rhorji,d_m,3);
        real_t d_coeff4ji = u_rhor_spline_view(t2rhorji,d_m,4);
        real_t d_coeff5ji = u_rhor_spline_view(t2rhorji,d_m,5);
        real_t d_coeff6ji = u_rhor_spline_view(t2rhorji,d_m,6);
        real_t d_rhoi = ((d_coeff3ji*d_p + d_coeff4ji)*d_p + d_coeff5ji)*d_p + d_coeff6ji;
        
        RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_rho[d_i], d_rhoi);
        if (d_newton_pair || d_j < d_nlocal)
        {
          int t2rhorij = u_type2rhor_view(d_itype, d_jtype);
          real_t d_coeff3ij = u_rhor_spline_view(t2rhorij,d_m,3);
          real_t d_coeff4ij = u_rhor_spline_view(t2rhorij,d_m,4);
          real_t d_coeff5ij = u_rhor_spline_view(t2rhorij,d_m,5);
          real_t d_coeff6ij = u_rhor_spline_view(t2rhorij,d_m,6);
          real_t d_rhoj = ((d_coeff3ij*d_p + d_coeff4ij)*d_p + d_coeff5ij)*d_p + d_coeff6ij;
          RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_rho[d_j], d_rhoj);
        }
      }
    }
  });

  gec(cudaMemcpy(h_rho, d_rho,  nmax * sizeof(real_t), cudaMemcpyDeviceToHost));
  
  // communicate and sum densities 
  // reverse_comm -> communicate and sum per-mpi rank rho
  if (newton_pair) comm->reverse_comm_pair(this);
  
  gec(cudaMemcpy(d_rho, h_rho,  nmax * sizeof(real_t), cudaMemcpyHostToDevice));
  // fp = derivative of embedding energy at each atom
  // phi = embedding energy at each atom
  // if rho > rhomax (e.g. due to close approach of two atoms),
  //   will exceed table, so add linear term to conserve energy

  RAJA::ReduceSum<RAJA::cuda_reduce, real_t> r_evdwl(eng_vdwl);
  RAJA::forall<RAJA::cuda_exec<CUDA_BLOCK_SIZE>>(RAJA::RangeSegment(0, inum), [=] RAJA_DEVICE (int ii) 
  {
    int d_i = d_ilist[ii];
    real_t d_p = d_rho[d_i] * d_rdrho + static_cast<real_t>(1.0);
    int d_m = static_cast<int> (d_p);
    d_m  = MAX(1,MIN(d_m,d_nrho-1));
    d_p -= d_m;
    d_p  = MIN(d_p, static_cast<real_t>(1.0));
    int tdi = d_type[d_i];
    int t2fr_tdi = u_type2frho_view(tdi); 
   
    real_t d_coeff0 = u_frho_spline_view( t2fr_tdi , d_m , 0 );
    real_t d_coeff1 = u_frho_spline_view( t2fr_tdi , d_m , 1 );
    real_t d_coeff2 = u_frho_spline_view( t2fr_tdi , d_m , 2 );
    real_t d_coeff3 = u_frho_spline_view( t2fr_tdi , d_m , 3 );
    real_t d_coeff4 = u_frho_spline_view( t2fr_tdi , d_m , 4 );
    real_t d_coeff5 = u_frho_spline_view( t2fr_tdi , d_m , 5 );
    real_t d_coeff6 = u_frho_spline_view( t2fr_tdi , d_m , 6 );

    real_t dfpi = (d_coeff0*d_p + d_coeff1)*d_p + d_coeff2;
    RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_fp[d_i], dfpi); 
    
    if (d_eflag)
    {
      real_t d_phi = ((d_coeff3*d_p + d_coeff4)*d_p + d_coeff5)*d_p + d_coeff6;
      
      if (d_rho[d_i] > d_rhomax) 
      {
        real_t nd_phi = d_fp[d_i] * (d_rho[d_i]-d_rhomax);
        RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_phi, nd_phi);
      }
      d_phi *= u_scale_view(tdi, tdi);
      if (d_eflag_global) r_evdwl += d_phi;
      if (d_eflag_atom) RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_eatom[d_i], d_phi);
    }
  });
  
  gec(cudaMemcpy(h_fp,   d_fp,  nmax * sizeof(real_t), cudaMemcpyDeviceToHost));
  
  eng_vdwl += static_cast<double> (r_evdwl.get());
  if (eflag_atom)
  {
    gec(cudaMemcpy(h_eatom, d_eatom, xfdim * sizeof(real_t), cudaMemcpyDeviceToHost));
    for (int i = 0; i < nmax; i++) eatom[i] = static_cast<double> (h_eatom[i]);
  }

  // communicate derivative of embedding function
  // forward_comm -> Communicates fp
  comm->forward_comm_pair(this);

  // compute forces on each atom
  // loop over neighbors of my atoms

  gec(cudaMemcpy(d_fp,   h_fp,  nmax * sizeof(real_t), cudaMemcpyHostToDevice));
  
  int &d_evflag = evflag;
  RAJA::ReduceSum<RAJA::cuda_reduce, real_t> d_evdwl(eng_vdwl);
  RAJA::forall<RAJA::cuda_exec<CUDA_BLOCK_SIZE>>(RAJA::RangeSegment(0, inum), [=] RAJA_DEVICE (int ii)
  {
    int d_i = d_ilist[ii];
    real_t d_xtmp = d_x_view(d_i,0);
    real_t d_ytmp = d_x_view(d_i,1);
    real_t d_ztmp = d_x_view(d_i,2);
    int d_itype = d_type[d_i];
    
    for (int jj = 0; jj < d_numneigh[d_i]; jj++) 
    {
      int d_j = d_firstneigh_view(d_i, jj);
      d_j &= NEIGHMASK;

      real_t d_delx = d_xtmp - d_x_view(d_j,0);
      real_t d_dely = d_ytmp - d_x_view(d_j,1);
      real_t d_delz = d_ztmp - d_x_view(d_j,2);
      real_t d_rsq  = d_delx*d_delx + d_dely*d_dely + d_delz*d_delz;

      if (d_rsq < d_cutforcesq) 
      {
        int d_jtype = d_type[d_j];
        real_t d_r = static_cast<real_t> (sqrt(d_rsq));
        real_t d_p = d_r*d_rdr + static_cast<real_t> (1.0);
        int d_m = static_cast<int> (d_p);
        d_m = MIN(d_m,d_nr-1);
        d_p -= d_m;
        d_p = MIN(d_p, static_cast<real_t> (1.0) );

        int d_t2rhor = u_type2rhor_view(d_itype,d_jtype);

        real_t d_coeff0rhor = u_rhor_spline_view(d_t2rhor,d_m,0);
        real_t d_coeff1rhor = u_rhor_spline_view(d_t2rhor,d_m,1);
        real_t d_coeff2rhor = u_rhor_spline_view(d_t2rhor,d_m,2);
        real_t d_rhoip = (d_coeff0rhor*d_p + d_coeff1rhor)*d_p + d_coeff2rhor;
        
        d_t2rhor = u_type2rhor_view(d_jtype,d_itype);
        d_coeff0rhor = u_rhor_spline_view(d_t2rhor,d_m,0);
        d_coeff1rhor = u_rhor_spline_view(d_t2rhor,d_m,1);
        d_coeff2rhor = u_rhor_spline_view(d_t2rhor,d_m,2);
        real_t d_rhojp = (d_coeff0rhor*d_p + d_coeff1rhor)*d_p + d_coeff2rhor;
        
        int d_t2z2r = u_type2z2r_view(d_itype,d_jtype);
        real_t d_coeff0z2r = u_z2r_spline_view(d_t2z2r,d_m,0);
        real_t d_coeff1z2r = u_z2r_spline_view(d_t2z2r,d_m,1);
        real_t d_coeff2z2r = u_z2r_spline_view(d_t2z2r,d_m,2);
        real_t d_coeff3z2r = u_z2r_spline_view(d_t2z2r,d_m,3);
        real_t d_coeff4z2r = u_z2r_spline_view(d_t2z2r,d_m,4);
        real_t d_coeff5z2r = u_z2r_spline_view(d_t2z2r,d_m,5);
        real_t d_coeff6z2r = u_z2r_spline_view(d_t2z2r,d_m,6);
        
        real_t d_z2p = (d_coeff0z2r*d_p + d_coeff1z2r)*d_p + d_coeff2z2r;
        real_t d_z2  = ((d_coeff3z2r*d_p + d_coeff4z2r)*d_p + d_coeff5z2r)*d_p + d_coeff6z2r;

        real_t d_recip = static_cast<real_t>(1.0)/d_r;
        real_t d_phi = d_z2*d_recip;
        real_t d_phip = d_z2p*d_recip - d_phi*d_recip;
        real_t d_psip = d_fp[d_i]*d_rhojp + d_fp[d_j]*d_rhoip + d_phip;
        real_t d_fpair = -u_scale_view(d_itype,d_jtype)*d_psip*d_recip;

        real_t d_xfpair = d_delx*d_fpair;
        real_t d_yfpair = d_dely*d_fpair;
        real_t d_zfpair = d_delz*d_fpair;

        RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_f_view(d_i,0), d_xfpair);
        RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_f_view(d_i,1), d_yfpair);
        RAJA::atomic::atomicAdd<RAJA::atomic::auto_atomic>(&d_f_view(d_i,2), d_zfpair);

        if (d_newton_pair || d_j < d_nlocal) 
        {
          RAJA::atomic::atomicSub<RAJA::atomic::auto_atomic>(&d_f_view(d_j,0), d_xfpair);
          RAJA::atomic::atomicSub<RAJA::atomic::auto_atomic>(&d_f_view(d_j,1), d_yfpair);
          RAJA::atomic::atomicSub<RAJA::atomic::auto_atomic>(&d_f_view(d_j,2), d_zfpair);
        }
        real_t myevdwl = static_cast<real_t> (0.0);
        if (d_eflag) myevdwl = u_scale_view(d_itype,d_jtype)*d_phi;
        if (d_evflag) 
        {
          d_evdwl += myevdwl;
        }
      }
    }
  });
  
  eng_vdwl = d_evdwl.get();
  
  gec(cudaMemcpy(h_f, d_f, xfdim * 3 * sizeof(real_t), cudaMemcpyDeviceToHost));
  for (int i = 0; i < xfdim; i++)
  {
    f[i][0] = static_cast<double> (h_f_view(i,0));
    f[i][1] = static_cast<double> (h_f_view(i,1));
    f[i][2] = static_cast<double> (h_f_view(i,2));
  }

  if (vflag_fdotr) virial_fdotr_compute();

  if (raja_allocated)
  {
    memory->destroy(h_rho);
    memory->destroy(h_fp);
    memory->destroy(h_x);
    memory->destroy(h_f);
  
    gec(cudaFree(d_x));
    gec(cudaFree(d_f));
    gec(cudaFree(d_rho));
    gec(cudaFree(d_fp));
    gec(cudaFree(d_type));   
    
    gec(cudaFree(d_ilist));   
    
    memory->destroy(h_firstneigh);
  
    gec(cudaFree(d_numneigh));
    gec(cudaFree(d_firstneigh));
    
    if(eflag_atom)
    {
      memory->destroy(h_eatom);
      gec(cudaFree(d_eatom)); 
    }
    
    raja_allocated=false;
  }
}

/* ----------------------------------------------------------------------
   allocate all arrays
------------------------------------------------------------------------- */

void PairEAM::allocate()
{
#ifdef RAJATRACE
  printf("PairEAM::allocate\n");
#endif
  allocated = 1;
  int n = atom->ntypes;

  memory->create(setflag,n+1,n+1,"pair:setflag");
  for (int i = 1; i <= n; i++)
    for (int j = i; j <= n; j++)
      setflag[i][j] = 0;

  memory->create(cutsq,n+1,n+1,"pair:cutsq");

  map = new int[n+1];
  for (int i = 1; i <= n; i++) map[i] = -1;

  type2frho = new int[n+1];
  memory->create(type2rhor,n+1,n+1,"pair:type2rhor");
  memory->create(type2z2r,n+1,n+1,"pair:type2z2r");
  memory->create(scale,n+1,n+1,"pair:scale");
  
  #ifdef RAJACUDA
  int np1 = n+1;
  gec(cudaMallocManaged((void **) &u_type2frho,       np1 * sizeof(int) ));
  gec(cudaMallocManaged((void **) &u_type2rhor, np1 * np1 * sizeof(int) ));
  gec(cudaMallocManaged((void **) &u_type2z2r , np1 * np1 * sizeof(int) ));
  
  cudaDeviceSynchronize();
  #endif
  
}

/* ----------------------------------------------------------------------
   global settings
------------------------------------------------------------------------- */

void PairEAM::settings(int narg, char **/*arg*/)
{
#ifdef RAJATRACE
  printf("PairEAM::settings\n");
#endif
  if (narg > 0) error->all(FLERR,"Illegal pair_style command");
}

/* ----------------------------------------------------------------------
   set coeffs for one or more type pairs
   read DYNAMO funcfl file
------------------------------------------------------------------------- */

void PairEAM::coeff(int narg, char **arg)
{
#ifdef RAJATRACE
  printf("PairEAM::coeff\n");
#endif
  if (!allocated) allocate();

  if (narg != 3) error->all(FLERR,"Incorrect args for pair coefficients");

  // parse pair of atom types

  int ilo,ihi,jlo,jhi;
  force->bounds(FLERR,arg[0],atom->ntypes,ilo,ihi);
  force->bounds(FLERR,arg[1],atom->ntypes,jlo,jhi);

  // read funcfl file if hasn't already been read
  // store filename in Funcfl data struct

  int ifuncfl;
  for (ifuncfl = 0; ifuncfl < nfuncfl; ifuncfl++)
    if (strcmp(arg[2],funcfl[ifuncfl].file) == 0) break;

  if (ifuncfl == nfuncfl) {
    nfuncfl++;
    funcfl = (Funcfl *)
      memory->srealloc(funcfl,nfuncfl*sizeof(Funcfl),"pair:funcfl");
    read_file(arg[2]);
    int n = strlen(arg[2]) + 1;
    funcfl[ifuncfl].file = new char[n];
    strcpy(funcfl[ifuncfl].file,arg[2]);
  }

  // set setflag and map only for i,i type pairs
  // set mass of atom type if i = j
  
  int count = 0;
  for (int i = ilo; i <= ihi; i++) {
    for (int j = MAX(jlo,i); j <= jhi; j++) {
      if (i == j) {
        setflag[i][i] = 1;
        map[i] = ifuncfl;
        atom->set_mass(FLERR,i,funcfl[ifuncfl].mass);
        count++;
      }
      scale[i][j] = 1.0;
    }
  }

  if (count == 0) error->all(FLERR,"Incorrect args for pair coefficients");
}

/* ----------------------------------------------------------------------
   init specific to this pair style
------------------------------------------------------------------------- */

void PairEAM::init_style()
{
#ifdef RAJATRACE
  printf("PairEAM::init_style\n");
#endif
  // convert read-in file(s) to arrays and spline them

  file2array();
  array2spline();

  neighbor->request(this,instance_me);
    //~ int inum = list->inum;
  //~ printf("inum = %d\n",inum);
  //~ exit(0);
}

/* ----------------------------------------------------------------------
   init for one type pair i,j and corresponding j,i
------------------------------------------------------------------------- */

double PairEAM::init_one(int i, int j)
{
#ifdef RAJATRACE
  printf("PairEAM::init_one\n");
#endif
  // single global cutoff = max of cut from all files read in
  // for funcfl could be multiple files
  // for setfl or fs, just one file

  if (setflag[i][j] == 0) scale[i][j] = 1.0;
  scale[j][i] = scale[i][j];
  
  if (funcfl) {
    cutmax = 0.0;
    for (int m = 0; m < nfuncfl; m++)
      cutmax = MAX(cutmax,funcfl[m].cut);
  } else if (setfl) cutmax = setfl->cut;
  else if (fs) cutmax = fs->cut;

  cutforcesq = cutmax*cutmax;


  return cutmax;
}

/* ----------------------------------------------------------------------
   read potential values from a DYNAMO single element funcfl file
------------------------------------------------------------------------- */

void PairEAM::read_file(char *filename)
{
#ifdef RAJATRACE
  printf("PairEAM::read_file\n");
#endif
  Funcfl *file = &funcfl[nfuncfl-1];

  int me = comm->me;
  FILE *fptr;
  char line[MAXLINE];

  if (me == 0) {
    fptr = force->open_potential(filename);
    if (fptr == NULL) {
      char str[128];
      snprintf(str,128,"Cannot open EAM potential file %s",filename);
      error->one(FLERR,str);
    }
  }

  int tmp,nwords;
  if (me == 0) {
    fgets(line,MAXLINE,fptr);
    fgets(line,MAXLINE,fptr);
    sscanf(line,"%d %lg",&tmp,&file->mass);
    fgets(line,MAXLINE,fptr);
    nwords = sscanf(line,"%d %lg %d %lg %lg",
           &file->nrho,&file->drho,&file->nr,&file->dr,&file->cut);
  }

  MPI_Bcast(&nwords,1,MPI_INT,0,world);
  MPI_Bcast(&file->mass,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&file->nrho,1,MPI_INT,0,world);
  MPI_Bcast(&file->drho,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&file->nr,1,MPI_INT,0,world);
  MPI_Bcast(&file->dr,1,MPI_DOUBLE,0,world);
  MPI_Bcast(&file->cut,1,MPI_DOUBLE,0,world);

  if ((nwords != 5) || (file->nrho <= 0) || (file->nr <= 0) || (file->dr <= 0.0))
    error->all(FLERR,"Invalid EAM potential file");

  memory->create(file->frho,(file->nrho+1),"pair:frho");
  memory->create(file->rhor,(file->nr+1),"pair:rhor");
  memory->create(file->zr,(file->nr+1),"pair:zr");

  if (me == 0) grab(fptr,file->nrho,&file->frho[1]);
  MPI_Bcast(&file->frho[1],file->nrho,MPI_DOUBLE,0,world);

  if (me == 0) grab(fptr,file->nr,&file->zr[1]);
  MPI_Bcast(&file->zr[1],file->nr,MPI_DOUBLE,0,world);

  if (me == 0) grab(fptr,file->nr,&file->rhor[1]);
  MPI_Bcast(&file->rhor[1],file->nr,MPI_DOUBLE,0,world);

  if (me == 0) fclose(fptr);
}

/* ----------------------------------------------------------------------
   convert read-in funcfl potential(s) to standard array format
   interpolate all file values to a single grid and cutoff
------------------------------------------------------------------------- */

void PairEAM::file2array()
{
#ifdef RAJATRACE
  printf("PairEAM::file2array\n");
#endif
  int i,j,k,m,n;
  int ntypes = atom->ntypes;
  double sixth = 1.0/6.0;

  // determine max function params from all active funcfl files
  // active means some element is pointing at it via map



  int active;
  double rmax;
  dr = drho = rmax = rhomax = 0.0;

  for (int i = 0; i < nfuncfl; i++) {
    active = 0;
    for (j = 1; j <= ntypes; j++)
      if (map[j] == i) active = 1;
    if (active == 0) continue;
    Funcfl *file = &funcfl[i];
    dr = MAX(dr,file->dr);
    drho = MAX(drho,file->drho);
    rmax = MAX(rmax,(file->nr-1) * file->dr);
    rhomax = MAX(rhomax,(file->nrho-1) * file->drho);
  }

  // set nr,nrho from cutoff and spacings
  // 0.5 is for round-off in divide

  nr = static_cast<int> (rmax/dr + 0.5);
  nrho = static_cast<int> (rhomax/drho + 0.5);

  // ------------------------------------------------------------------
  // setup frho arrays
  // ------------------------------------------------------------------

  // allocate frho arrays
  // nfrho = # of funcfl files + 1 for zero array

  nfrho = nfuncfl + 1;
  memory->destroy(frho);
  memory->create(frho,nfrho,nrho+1,"pair:frho");

  // interpolate each file's frho to a single grid and cutoff

  double r,p,cof1,cof2,cof3,cof4;

  n = 0;
  for (i = 0; i < nfuncfl; i++) {
    Funcfl *file = &funcfl[i];
    for (m = 1; m <= nrho; m++) {
      r = (m-1)*drho;
      p = r/file->drho + 1.0;
      k = static_cast<int> (p);
      k = MIN(k,file->nrho-2);
      k = MAX(k,2);
      p -= k;
      p = MIN(p,2.0);
      cof1 = -sixth*p*(p-1.0)*(p-2.0);
      cof2 = 0.5*(p*p-1.0)*(p-2.0);
      cof3 = -0.5*p*(p+1.0)*(p-2.0);
      cof4 = sixth*p*(p*p-1.0);
      frho[n][m] = cof1*file->frho[k-1] + cof2*file->frho[k] +
        cof3*file->frho[k+1] + cof4*file->frho[k+2];
    }
    n++;
  }

  // add extra frho of zeroes for non-EAM types to point to (pair hybrid)
  // this is necessary b/c fp is still computed for non-EAM atoms

  for (m = 1; m <= nrho; m++) frho[nfrho-1][m] = 0.0;

  // type2frho[i] = which frho array (0 to nfrho-1) each atom type maps to
  // if atom type doesn't point to file (non-EAM atom in pair hybrid)
  // then map it to last frho array of zeroes

  for (i = 1; i <= ntypes; i++)
    if (map[i] >= 0) type2frho[i] = map[i];
    else type2frho[i] = nfrho-1;

  // ------------------------------------------------------------------
  // setup rhor arrays
  // ------------------------------------------------------------------

  // allocate rhor arrays
  // nrhor = # of funcfl files

  nrhor = nfuncfl;
  memory->destroy(rhor);
  memory->create(rhor,nrhor,nr+1,"pair:rhor");

  // interpolate each file's rhor to a single grid and cutoff

  n = 0;
  for (i = 0; i < nfuncfl; i++) {
    Funcfl *file = &funcfl[i];
    for (m = 1; m <= nr; m++) {
      r = (m-1)*dr;
      p = r/file->dr + 1.0;
      k = static_cast<int> (p);
      k = MIN(k,file->nr-2);
      k = MAX(k,2);
      p -= k;
      p = MIN(p,2.0);
      cof1 = -sixth*p*(p-1.0)*(p-2.0);
      cof2 = 0.5*(p*p-1.0)*(p-2.0);
      cof3 = -0.5*p*(p+1.0)*(p-2.0);
      cof4 = sixth*p*(p*p-1.0);
      rhor[n][m] = cof1*file->rhor[k-1] + cof2*file->rhor[k] +
        cof3*file->rhor[k+1] + cof4*file->rhor[k+2];
    }
    n++;
  }

  // type2rhor[i][j] = which rhor array (0 to nrhor-1) each type pair maps to
  // for funcfl files, I,J mapping only depends on I
  // OK if map = -1 (non-EAM atom in pair hybrid) b/c type2rhor not used

  for (i = 1; i <= ntypes; i++)
    for (j = 1; j <= ntypes; j++)
      type2rhor[i][j] = map[i];

  // ------------------------------------------------------------------
  // setup z2r arrays
  // ------------------------------------------------------------------

  // allocate z2r arrays
  // nz2r = N*(N+1)/2 where N = # of funcfl files

  nz2r = nfuncfl*(nfuncfl+1)/2;
  memory->destroy(z2r);
  memory->create(z2r,nz2r,nr+1,"pair:z2r");

  // create a z2r array for each file against other files, only for I >= J
  // interpolate zri and zrj to a single grid and cutoff

  double zri,zrj;

  n = 0;
  for (i = 0; i < nfuncfl; i++) {
    Funcfl *ifile = &funcfl[i];
    for (j = 0; j <= i; j++) {
      Funcfl *jfile = &funcfl[j];

      for (m = 1; m <= nr; m++) {
        r = (m-1)*dr;

        p = r/ifile->dr + 1.0;
        k = static_cast<int> (p);
        k = MIN(k,ifile->nr-2);
        k = MAX(k,2);
        p -= k;
        p = MIN(p,2.0);
        cof1 = -sixth*p*(p-1.0)*(p-2.0);
        cof2 = 0.5*(p*p-1.0)*(p-2.0);
        cof3 = -0.5*p*(p+1.0)*(p-2.0);
        cof4 = sixth*p*(p*p-1.0);
        zri = cof1*ifile->zr[k-1] + cof2*ifile->zr[k] +
          cof3*ifile->zr[k+1] + cof4*ifile->zr[k+2];

        p = r/jfile->dr + 1.0;
        k = static_cast<int> (p);
        k = MIN(k,jfile->nr-2);
        k = MAX(k,2);
        p -= k;
        p = MIN(p,2.0);
        cof1 = -sixth*p*(p-1.0)*(p-2.0);
        cof2 = 0.5*(p*p-1.0)*(p-2.0);
        cof3 = -0.5*p*(p+1.0)*(p-2.0);
        cof4 = sixth*p*(p*p-1.0);
        zrj = cof1*jfile->zr[k-1] + cof2*jfile->zr[k] +
          cof3*jfile->zr[k+1] + cof4*jfile->zr[k+2];

        z2r[n][m] = 27.2*0.529 * zri*zrj;
      }
      n++;
    }
  }

  // type2z2r[i][j] = which z2r array (0 to nz2r-1) each type pair maps to
  // set of z2r arrays only fill lower triangular Nelement matrix
  // value = n = sum over rows of lower-triangular matrix until reach irow,icol
  // swap indices when irow < icol to stay lower triangular
  // if map = -1 (non-EAM atom in pair hybrid):
  //   type2z2r is not used by non-opt
  //   but set type2z2r to 0 since accessed by opt

  int irow,icol;
  for (i = 1; i <= ntypes; i++) {
    for (j = 1; j <= ntypes; j++) {
      irow = map[i];
      icol = map[j];
      if (irow == -1 || icol == -1) {
        type2z2r[i][j] = 0;
        continue;
      }
      if (irow < icol) {
        irow = map[j];
        icol = map[i];
      }
      n = 0;
      for (m = 0; m < irow; m++) n += m + 1;
      n += icol;
      type2z2r[i][j] = n;
    }
  }
}

/* ---------------------------------------------------------------------- */

void PairEAM::array2spline()
{
#ifdef RAJATRACE
  printf("PairEAM::array2spline\n");
#endif
  rdr = 1.0/dr;
  rdrho = 1.0/drho;

  memory->destroy(frho_spline);
  memory->destroy(rhor_spline);
  memory->destroy(z2r_spline);

  memory->create(frho_spline,nfrho,nrho+1,7,"pair:frho");
  memory->create(rhor_spline,nrhor,nr+1,7,"pair:rhor");
  memory->create(z2r_spline,nz2r,nr+1,7,"pair:z2r");

  for (int i = 0; i < nfrho; i++)
    interpolate(nrho,drho,frho[i],frho_spline[i]);

  for (int i = 0; i < nrhor; i++)
    interpolate(nr,dr,rhor[i],rhor_spline[i]);

  for (int i = 0; i < nz2r; i++)
    interpolate(nr,dr,z2r[i],z2r_spline[i]);
    
  int Xfrho = nfrho,
      Yfrho = nrho+1,
      Xrhor = nrhor,
      Yrhor = nr+1,
      Xz2r  = nz2r,
      Yz2r  = nr+1,
      Z     = 7;
  
  int nfrho_spline = Xfrho * Yfrho * Z * static_cast<int> (sizeof(real_t));
  int nrhor_spline = Xrhor * Yrhor * Z * static_cast<int> (sizeof(real_t));
  int nz2r_spline  = Xz2r  * Yz2r  * Z * static_cast<int> (sizeof(real_t));
  
  if (raja_allocated)
  {
    gec(cudaFree(u_frho_spline));
    gec(cudaFree(u_rhor_spline));
    gec(cudaFree(u_z2r_spline));
    raja_allocated = false;
  }
    
  if (!raja_allocated)
  {
    gec(cudaMallocManaged((void **) &u_frho_spline, nfrho_spline));
    gec(cudaMallocManaged((void **) &u_rhor_spline, nrhor_spline));
    gec(cudaMallocManaged((void **) &u_z2r_spline,  nz2r_spline ));
    cudaDeviceSynchronize();
    raja_allocated = true;
  }
  
  RAJA::View<real_t, RAJA::Layout<3>> u_frho_spline_view(u_frho_spline, Xfrho, Yfrho, Z);
  RAJA::View<real_t, RAJA::Layout<3>> u_rhor_spline_view(u_rhor_spline, Xrhor, Yrhor, Z);
  RAJA::View<real_t, RAJA::Layout<3>> u_z2r_spline_view (u_z2r_spline , Xz2r , Yz2r , Z);
  
  for (int ix = 0; ix < Xfrho; ix++)
  {
    for (int iy = 0; iy < Yfrho; iy++)
    {
      u_frho_spline_view(ix, iy, 0) = static_cast<real_t> (frho_spline[ix][iy][0]);
      u_frho_spline_view(ix, iy, 1) = static_cast<real_t> (frho_spline[ix][iy][1]);
      u_frho_spline_view(ix, iy, 2) = static_cast<real_t> (frho_spline[ix][iy][2]);
      u_frho_spline_view(ix, iy, 3) = static_cast<real_t> (frho_spline[ix][iy][3]);
      u_frho_spline_view(ix, iy, 4) = static_cast<real_t> (frho_spline[ix][iy][4]);
      u_frho_spline_view(ix, iy, 5) = static_cast<real_t> (frho_spline[ix][iy][5]);
      u_frho_spline_view(ix, iy, 6) = static_cast<real_t> (frho_spline[ix][iy][6]);
    }
  }
  
  for (int ix = 0; ix < Xrhor; ix++)
  {
    for (int iy = 0; iy < Yrhor; iy++)
    {
      u_rhor_spline_view(ix, iy, 0) = static_cast<real_t> (rhor_spline[ix][iy][0]);
      u_rhor_spline_view(ix, iy, 1) = static_cast<real_t> (rhor_spline[ix][iy][1]);
      u_rhor_spline_view(ix, iy, 2) = static_cast<real_t> (rhor_spline[ix][iy][2]);
      u_rhor_spline_view(ix, iy, 3) = static_cast<real_t> (rhor_spline[ix][iy][3]);
      u_rhor_spline_view(ix, iy, 4) = static_cast<real_t> (rhor_spline[ix][iy][4]);
      u_rhor_spline_view(ix, iy, 5) = static_cast<real_t> (rhor_spline[ix][iy][5]);
      u_rhor_spline_view(ix, iy, 6) = static_cast<real_t> (rhor_spline[ix][iy][6]);
    }
  }
  
  for (int ix = 0; ix < Xz2r; ix++)
  {
    for (int iy = 0; iy < Yz2r; iy++)
    {
      u_z2r_spline_view(ix, iy, 0) = static_cast<real_t> (z2r_spline[ix][iy][0]);
      u_z2r_spline_view(ix, iy, 1) = static_cast<real_t> (z2r_spline[ix][iy][1]);
      u_z2r_spline_view(ix, iy, 2) = static_cast<real_t> (z2r_spline[ix][iy][2]);
      u_z2r_spline_view(ix, iy, 3) = static_cast<real_t> (z2r_spline[ix][iy][3]);
      u_z2r_spline_view(ix, iy, 4) = static_cast<real_t> (z2r_spline[ix][iy][4]);
      u_z2r_spline_view(ix, iy, 5) = static_cast<real_t> (z2r_spline[ix][iy][5]);
      u_z2r_spline_view(ix, iy, 6) = static_cast<real_t> (z2r_spline[ix][iy][6]);
    }
  }
}

/* ---------------------------------------------------------------------- */

void PairEAM::interpolate(int n, double delta, double *f, double **spline)
{
#ifdef RAJATRACE
  printf("PairEAM::interpolate\n");
#endif
  for (int m = 1; m <= n; m++) spline[m][6] = f[m];

  spline[1][5] = spline[2][6] - spline[1][6];
  spline[2][5] = 0.5 * (spline[3][6]-spline[1][6]);
  spline[n-1][5] = 0.5 * (spline[n][6]-spline[n-2][6]);
  spline[n][5] = spline[n][6] - spline[n-1][6];

  for (int m = 3; m <= n-2; m++)
    spline[m][5] = ((spline[m-2][6]-spline[m+2][6]) +
                    8.0*(spline[m+1][6]-spline[m-1][6])) / 12.0;

  for (int m = 1; m <= n-1; m++) {
    spline[m][4] = 3.0*(spline[m+1][6]-spline[m][6]) -
      2.0*spline[m][5] - spline[m+1][5];
    spline[m][3] = spline[m][5] + spline[m+1][5] -
      2.0*(spline[m+1][6]-spline[m][6]);
  }

  spline[n][4] = 0.0;
  spline[n][3] = 0.0;

  for (int m = 1; m <= n; m++) {
    spline[m][2] = spline[m][5]/delta;
    spline[m][1] = 2.0*spline[m][4]/delta;
    spline[m][0] = 3.0*spline[m][3]/delta;
  }
}

/* ----------------------------------------------------------------------
   grab n values from file fp and put them in list
   values can be several to a line
   only called by proc 0
------------------------------------------------------------------------- */

void PairEAM::grab(FILE *fptr, int n, double *list)
{
#ifdef RAJATRACE
  printf("PairEAM::grab\n");
#endif

  char *ptr;
  char line[MAXLINE];

  int i = 0;
  while (i < n) {
    fgets(line,MAXLINE,fptr);
    ptr = strtok(line," \t\n\r\f");
    list[i++] = atof(ptr);
    while ((ptr = strtok(NULL," \t\n\r\f"))) list[i++] = atof(ptr);
  }
}

/* ---------------------------------------------------------------------- */

double PairEAM::single(int i, int j, int itype, int jtype,
                       double rsq, double /*factor_coul*/, double /*factor_lj*/,
                       double &fforce)
{
#ifdef RAJATRACE
  printf("PairEAM::single\n");
#endif
  int m;
  double r,p,rhoip,rhojp,z2,z2p,recip,phi,phip,psip;
  double *coeff;

  r = sqrt(rsq);
  p = r*rdr + 1.0;
  m = static_cast<int> (p);
  m = MIN(m,nr-1);
  p -= m;
  p = MIN(p,1.0);

  coeff = rhor_spline[type2rhor[itype][jtype]][m];
  rhoip = (coeff[0]*p + coeff[1])*p + coeff[2];
  coeff = rhor_spline[type2rhor[jtype][itype]][m];
  rhojp = (coeff[0]*p + coeff[1])*p + coeff[2];
  coeff = z2r_spline[type2z2r[itype][jtype]][m];
  z2p = (coeff[0]*p + coeff[1])*p + coeff[2];
  z2 = ((coeff[3]*p + coeff[4])*p + coeff[5])*p + coeff[6];

  recip = 1.0/r;
  phi = z2*recip;
  phip = z2p*recip - phi*recip;
  psip = fp[i]*rhojp + fp[j]*rhoip + phip;
  fforce = -psip*recip;

  return phi;
}

/* ---------------------------------------------------------------------- */

int PairEAM::pack_forward_comm(int n, int *list, double *buf,
                               int /*pbc_flag*/, int * /*pbc*/)
{
#ifdef RAJATRACE
  printf("PairEAM::pack_forward_comm\n");
#endif
  int i,j,m;

  m = 0;
  for (i = 0; i < n; i++) {
    j = list[i];
    buf[m++] = h_fp[j];
  }
  return m;
}

/* ---------------------------------------------------------------------- */

void PairEAM::unpack_forward_comm(int n, int first, double *buf)
{
#ifdef RAJATRACE
  printf("PairEAM::unpack_forward_comm\n");
#endif
  int i,m,last;

  m = 0;
  last = first + n;
  for (i = first; i < last; i++) h_fp[i] = buf[m++];
}

/* ---------------------------------------------------------------------- */

int PairEAM::pack_reverse_comm(int n, int first, double *buf)
{
#ifdef RAJATRACE
  printf("PairEAM::pack_reverse_comm\n");
#endif
  int i,m,last;

  m = 0;
  last = first + n;
  for (i = first; i < last; i++)
  {
    buf[m++] = static_cast<double> (h_rho[i]);
  }
  return m;
}

/* ---------------------------------------------------------------------- */

void PairEAM::unpack_reverse_comm(int n, int *list, double *buf)
{
#ifdef RAJATRACE
  printf("PairEAM::unpack_reverse_comm\n");
#endif
  int i,j,m;

  m = 0;
  for (i = 0; i < n; i++) {
    j = list[i];
    h_rho[j] += buf[m++];
  }
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based arrays
------------------------------------------------------------------------- */

double PairEAM::memory_usage()
{
#ifdef RAJATRACE
  printf("PairEAM::memory_usage\n");
#endif
  double bytes = maxeatom * sizeof(double);
  bytes += maxvatom*6 * sizeof(double);
  bytes += 2 * nmax * sizeof(double);
  return bytes;
}

/* ----------------------------------------------------------------------
   swap fp array with one passed in by caller
------------------------------------------------------------------------- */
void PairEAM::swap_eam(real_t *fp_caller, real_t **fp_caller_hold)
{
#ifdef RAJATRACE
  printf("PairEAM::swap_eam\n");
#endif

  real_t *tmp = h_fp;
  h_fp = fp_caller;
  *fp_caller_hold = tmp;
}

/* ---------------------------------------------------------------------- */

void *PairEAM::extract(const char *str, int &dim)
{
#ifdef RAJATRACE
  printf("PairEAM::extract\n");
#endif
  dim = 2;
  if (strcmp(str,"scale") == 0) return (void *) scale;
  return NULL;
}
