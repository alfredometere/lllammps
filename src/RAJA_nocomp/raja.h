/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef RAJA_LMP_H
#define RAJA_LMP_H

#include <cuda.h>
#include <cuda_runtime.h>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


#include "pointers.h"
#include "raja_type.h"


namespace LAMMPS_NS {

class RajaLMP : protected Pointers {
 public:
  int raja_exists;
  int neighflag;
  int neighflag_qeq;
  int neighflag_qeq_set;
  int exchange_comm_classic;
  int forward_comm_classic;
  int reverse_comm_classic;
  int exchange_comm_on_host;
  int forward_comm_on_host;
  int reverse_comm_on_host;
  int num_threads,ngpu;
  int numa;
  int auto_sync;
  int gpu_direct_flag;

  RajaLMP(class LAMMPS *, int, char **);
  ~RajaLMP();
  void accelerator(int, char **);
  int neigh_count(int);

 private:
  static void my_signal_handler(int);
};

}

#endif

/* ERROR/WARNING messages:

E: Invalid Raja command-line args

Self-explanatory.  See Section 2.7 of the manual for details.

E: GPUs are requested but Raja has not been compiled for CUDA

Recompile Raja with CUDA support to use GPUs.

E: Raja has been compiled for CUDA but no GPUs are requested

One or more GPUs must be used when Raja is compiled for CUDA.

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

U: Must use Raja half/thread or full neighbor list with threads or GPUs

Using Raja half-neighbor lists with threading is not allowed.

*/
