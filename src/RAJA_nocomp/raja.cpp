/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <mpi.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cctype>
#include <csignal>
#include <unistd.h>
#include "raja.h"
#include "lammps.h"
#include "force.h"
#include "neighbor.h" // Change to neighbor_raja.h when implemented
#include "neigh_list.h"
//#include "neighbor_raja.h"
//#include "neigh_list_raja.h"
#include "error.h"
// #include "memory_raja.h"
#include "memory.h"

// This interface will assume CUDA-awareness
int cuda_aware_flag = 1;

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

RajaLMP::RajaLMP(LAMMPS *lmp, int narg, char **arg) : Pointers(lmp)
{
  raja_exists = 1;
  lmp->raja = this;

  //delete memory;
  //memory = new Memory(lmp);
  // memoryRJ = (MemoryRaja*) memory;
  // memoryRJ = (Memory*) memory;

  auto_sync = 1;

  int me = 0;
  MPI_Comm_rank(world,&me);
  if (me == 0) error->message(FLERR,"RAJA mode is enabled");

  // process any command-line args that invoke Raja settings

  ngpu = 0;
  int device = 0;
  num_threads = 1;
  numa = 1;

  //cudaGetDeviceCount(&ngpu);

  int iarg = 0;
  while (iarg < narg) {
    if (strcmp(arg[iarg],"d") == 0 || strcmp(arg[iarg],"device") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Invalid Raja command-line args");
      device = atoi(arg[iarg+1]);
      iarg += 2;

    } else if (strcmp(arg[iarg],"g") == 0 ||
               strcmp(arg[iarg],"gpus") == 0) {
#ifndef RAJA_ENABLE_CUDA
      error->all(FLERR,"GPUs are requested but RAJA has not been compiled for CUDA");
#endif
      if (iarg+2 > narg) error->all(FLERR,"Invalid RAJA command-line args");
      ngpu = atoi(arg[iarg+1]);

      int skip_gpu = 9999;
      if (iarg+2 < narg && isdigit(arg[iarg+2][0])) {
        skip_gpu = atoi(arg[iarg+2]);
        iarg++;
      }
      iarg += 2;

      char *str;
      if ((str = getenv("SLURM_LOCALID"))) {
        int local_rank = atoi(str);
        device = local_rank % ngpu;
        if (device >= skip_gpu) device++;
      }
      if ((str = getenv("MV2_COMM_WORLD_LOCAL_RANK"))) {
        int local_rank = atoi(str);
        device = local_rank % ngpu;
        if (device >= skip_gpu) device++;
      }
      if ((str = getenv("OMPI_COMM_WORLD_LOCAL_RANK"))) {
        int local_rank = atoi(str);
        device = local_rank % ngpu;
        if (device >= skip_gpu) device++;
      }

    } else if (strcmp(arg[iarg],"t") == 0 ||
               strcmp(arg[iarg],"threads") == 0) {
      num_threads = atoi(arg[iarg+1]);
      iarg += 2;

    } else if (strcmp(arg[iarg],"n") == 0 ||
               strcmp(arg[iarg],"numa") == 0) {
      numa = atoi(arg[iarg+1]);
      iarg += 2;

    } else error->all(FLERR,"Invalid RAJA command-line args");
  }

  // initialize Raja

  if (me == 0) {
    if (screen) fprintf(screen,"  will use up to %d GPU(s) per node\n",ngpu);
    if (logfile) fprintf(logfile,"  will use up to %d GPU(s) per node\n",ngpu);
  }

#ifdef RAJA_ENABLE_CUDA
  if (ngpu <= 0)
    error->all(FLERR,"RAJA has been compiled for CUDA but no GPUs are requested");

  // check and warn about GPU-direct availability when using multiple MPI tasks

  int nmpi = 0;
  MPI_Comm_size(world,&nmpi);

  /*
  if ((nmpi > 1) && (me == 0)) {
    if ( 1 == have_cuda_awareness() ) {
      ; // all good, nothing to warn about
    } else if (-1 == have_cuda_awareness() ) {
      error->warning(FLERR,"RAJA with CUDA assumes GPUdirect is available,"
                     " but cannot determine if this is the case\n         try"
                     " '-pk raja gpu/direct off' when getting segmentation faults");
    } else if ( 0 == have_cuda_awareness() ) {
      error->warning(FLERR,"GPU-direct is NOT available, "
                     "using '-pk raja cuda/aware off' by default");
    } else {
      ; // should never get here
    }
  }
  */
#endif

  //Raja::InitArguments args;
  //args.num_threads = num_threads;
  //args.num_numa = numa;
  //args.device_id = device;

  //Raja::initialize(args);

  // default settings for package raja command

  neighflag = FULL;
  neighflag_qeq = FULL;
  neighflag_qeq_set = 0;
  exchange_comm_classic = 0;
  forward_comm_classic = 0;
  reverse_comm_classic = 0;
  exchange_comm_on_host = 0;
  forward_comm_on_host = 0;
  reverse_comm_on_host = 0;
  cuda_aware_flag = 1;


#ifdef KILL_RAJA_ON_SIGSEGV
  signal(SIGSEGV, my_signal_handler);
#endif
}

/* ---------------------------------------------------------------------- */

RajaLMP::~RajaLMP()
{
  // finalize Raja

  //Raja::finalize();
}

/* ----------------------------------------------------------------------
   invoked by package raja command
------------------------------------------------------------------------- */

void RajaLMP::accelerator(int narg, char **arg)
{
  // defaults

  neighflag = FULL;
  neighflag_qeq = FULL;
  neighflag_qeq_set = 0;
  int newtonflag = 0;
  double binsize = 0.0;
  exchange_comm_classic = forward_comm_classic = reverse_comm_classic = 0;
  exchange_comm_on_host = forward_comm_on_host = reverse_comm_on_host = 0;
  cuda_aware_flag = 1;

  int iarg = 0;
  while (iarg < narg) {
    if (strcmp(arg[iarg],"neigh") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"full") == 0) neighflag = FULL;
      else if (strcmp(arg[iarg+1],"half") == 0) {
        if (num_threads > 1 || ngpu > 0)
          neighflag = HALFTHREAD;
        else
          neighflag = HALF;
      } else if (strcmp(arg[iarg+1],"n2") == 0) neighflag = N2;
      else error->all(FLERR,"Illegal package raja command");
      if (!neighflag_qeq_set) neighflag_qeq = neighflag;
      iarg += 2;
    } else if (strcmp(arg[iarg],"neigh/qeq") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"full") == 0) neighflag_qeq = FULL;
      else if (strcmp(arg[iarg+1],"half") == 0) {
        if (num_threads > 1 || ngpu > 0)
          neighflag_qeq = HALFTHREAD;
        else
          neighflag_qeq = HALF;
      } else if (strcmp(arg[iarg+1],"n2") == 0) neighflag_qeq = N2;
      else error->all(FLERR,"Illegal package raja command");
      neighflag_qeq_set = 1;
      iarg += 2;
    } else if (strcmp(arg[iarg],"binsize") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      binsize = force->numeric(FLERR,arg[iarg+1]);
      iarg += 2;
    } else if (strcmp(arg[iarg],"newton") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"off") == 0) newtonflag = 0;
      else if (strcmp(arg[iarg+1],"on") == 0) newtonflag = 1;
      else error->all(FLERR,"Illegal package raja command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"comm") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"no") == 0) {
        exchange_comm_classic = forward_comm_classic = reverse_comm_classic = 1;
        exchange_comm_on_host = forward_comm_on_host = reverse_comm_on_host = 0;
      } else if (strcmp(arg[iarg+1],"host") == 0) {
        exchange_comm_classic = forward_comm_classic = reverse_comm_classic = 0;
        exchange_comm_on_host = forward_comm_on_host = reverse_comm_on_host = 1;
      } else if (strcmp(arg[iarg+1],"device") == 0) {
        exchange_comm_classic = forward_comm_classic = reverse_comm_classic = 0;
        exchange_comm_on_host = forward_comm_on_host = reverse_comm_on_host = 0;
      } else error->all(FLERR,"Illegal package raja command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"comm/exchange") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"no") == 0) exchange_comm_classic = 1;
      else if (strcmp(arg[iarg+1],"host") == 0) {
        exchange_comm_classic = 0;
        exchange_comm_on_host = 1;
      } else if (strcmp(arg[iarg+1],"device") == 0) {
        exchange_comm_classic = 0;
        exchange_comm_on_host = 0;
      } else error->all(FLERR,"Illegal package raja command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"comm/forward") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"no") == 0) forward_comm_classic = 1;
      else if (strcmp(arg[iarg+1],"host") == 0) {
        forward_comm_classic = 0;
        forward_comm_on_host = 1;
      } else if (strcmp(arg[iarg+1],"device") == 0) {
        forward_comm_classic = 0;
        forward_comm_on_host = 0;
      } else error->all(FLERR,"Illegal package raja command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"comm/reverse") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"no") == 0) reverse_comm_classic = 1;
      else if (strcmp(arg[iarg+1],"host") == 0) {
        reverse_comm_classic = 0;
        reverse_comm_on_host = 1;
      } else if (strcmp(arg[iarg+1],"device") == 0) {
        reverse_comm_classic = 0;
        reverse_comm_on_host = 0;
      } else error->all(FLERR,"Illegal package raja command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"cuda/aware") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal package raja command");
      if (strcmp(arg[iarg+1],"off") == 0) cuda_aware_flag = 0;
      else if (strcmp(arg[iarg+1],"on") == 0) cuda_aware_flag = 1;
      else error->all(FLERR,"Illegal package raja command");
      iarg += 2;
    } else error->all(FLERR,"Illegal package raja command");
  }

  // if "cuda/aware off" and "comm device", change to "comm host"

  // if (!cuda_aware_flag) {
   if (exchange_comm_classic == 0 && exchange_comm_on_host == 0)
     exchange_comm_on_host = 1;
   if (forward_comm_classic == 0 && forward_comm_on_host == 0)
     forward_comm_on_host = 1;
   if (reverse_comm_classic == 0 && reverse_comm_on_host == 0)
     reverse_comm_on_host = 1;
  // }

  // set newton flags
  // set neighbor binsize, same as neigh_modify command

  force->newton = force->newton_pair = force->newton_bond = newtonflag;

  neighbor->binsize_user = binsize;
  if (binsize <= 0.0) neighbor->binsizeflag = 0;
  else neighbor->binsizeflag = 1;
}

/* ----------------------------------------------------------------------
   called by Finish
------------------------------------------------------------------------- */

void RajaLMP::my_signal_handler(int sig)
{
  if (sig == SIGSEGV) {
    kill(getpid(),SIGABRT);
  }
}
