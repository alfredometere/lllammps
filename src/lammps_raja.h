#ifndef LAMMPS_RAJA_H
#define LAMMPS_RAJA_H

#if defined LMP_RAJA && defined LMP_RAJA_CUDA
#ifndef CUDA_BLOCK_SIZE
#define CUDA_BLOCK_SIZE 1024
#endif

#ifndef RAJACUDA
#define RAJACUDA
#endif

// To enable function tracing, uncomment the line below
//~ #define RAJATRACE

#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda_profiler_api.h>
#include "umpire/ResourceManager.hpp"
#include "umpire/strategy/DynamicPool.hpp"
//~ #include "chai/ManagedArray.hpp"
#include "RAJA/RAJA.hpp"

#define gec(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

#ifdef RAJASINGLE
typedef float real_t;
#define sqrtr(a) sqrtf(a)
#else
typedef double real_t;
#define sqrtr(a) sqrt(a)
#endif

#endif

#endif // LAMMPS_RAJA_H
