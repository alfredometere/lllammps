# Install/unInstall package files in LAMMPS
# mode = 0/1/2 for uninstall/install/update

mode=$1

# enforce using portable C locale
LC_ALL=C
export LC_ALL

# arg1 = file, arg2 = file it depends on

action () {
  if (test $mode = 0) then
    rm -f ../$1
  elif (! cmp -s $1 ../$1) then
    if (test -z "$2" || test -e ../$2) then
      cp $1 ..
      if (test $mode = 2) then
        echo "  updating src/$1"
      fi
    fi
  elif (test -n "$2") then
    if (test ! -e ../$2) then
      rm -f ../$1
    fi
  fi
}

# force rebuild of files with LMP_RAJA switch

RAJA_INSTALLED=0
if (test -e ../Makefile.package) then
  RAJA_INSTALLED=`grep DLMP_RAJA ../Makefile.package | wc -l`
fi 

if (test $mode = 1) then
  if (test $RAJA_INSTALLED = 0) then
    touch ../accelerator_raja.h
  fi
elif (test $mode = 0) then
  if (test $RAJA_INSTALLED = 1) then
    touch ../accelerator_raja.h
  fi
fi



#~ mkdir -p /home/metere1/lllammps/lib/CHAI/build
#~ cd /home/metere1/lllammps/lib/CHAI/build
#~ rm -rf /home/metere1/lllammps/lib/CHAI/build/*
#~ cmake -DENABLE_CUDA=ON -DENABLE_MPI=ON -DENABLE_OPENMP=ON -DENABLE_BENCHMARKS=OFF -DENABLE_TESTS=OFF -DENABLE_UM=ON ../
#~ make -j

mkdir -p /home/metere1/lllammps/lib/Umpire/build
cd /home/metere1/lllammps/lib/Umpire/build
rm -rf /home/metere1/lllammps/lib/Umpire/build/*
cmake -DENABLE_CUDA=ON -DENABLE_MPI=ON -DENABLE_OPENMP=ON -DENABLE_BENCHMARKS=OFF -DENABLE_EXAMPLES=OFF -DENABLE_TESTS=OFF ../
make -j

mkdir -p /home/metere1/lllammps/lib/RAJA/build
cd /home/metere1/lllammps/lib/RAJA/build
rm -rf /home/metere1/lllammps/lib/RAJA/build/*
#~ cmake -DENABLE_CUDA=ON -DENABLE_MPI=ON -DCUDA_ARCH=sm_61 -DENABLE_EXERCISES=OFF -DENABLE_EXAMPLES=OFF -DENABLE_CHAI=ON -Dumpire_DIR=/home/metere1/lllammps/lib/CHAI/build/src/tpl/umpire -Dchai_DIR=/home/metere1/lllammps/lib/CHAI/build/share/chai/cmake ../
cmake -DENABLE_CUDA=ON -DENABLE_MPI=ON -DCUDA_ARCH=sm_61 -DENABLE_EXERCISES=OFF -DENABLE_EXAMPLES=OFF ../
make -j





#~ cp /home/metere1/lllammps/src/RAJA/*.cpp /home/metere1/lllammps/src
#~ cp /home/metere1/lllammps/src/RAJA/*.h   /home/metere1/lllammps/src

#~ if (test $1 = 1) then

  #~ if (test -e ../Makefile.package) then
    #~ sed -i -e 's/[^ \t]*raja[^ \t]* //g' ../Makefile.package
    #~ sed -i -e 's/[^ \t]*RAJA[^ \t]* //g' ../Makefile.package
    #~ sed -i -e 's|^PKG_INC =[ \t]*|&-DLMP_RAJA |' ../Makefile.package
#~ #    sed -i -e 's|^PKG_PATH =[ \t]*|&-L..\/..\/lib\/RAJA\/core\/src |' ../Makefile.package
    #~ sed -i -e 's|^PKG_CPP_DEPENDS =[ \t]*|&$(RAJA_CPP_DEPENDS) |' ../Makefile.package
    #~ sed -i -e 's|^PKG_LIB =[ \t]*|&$(RAJA_LIBS) |' ../Makefile.package
    #~ sed -i -e 's|^PKG_LINK_DEPENDS =[ \t]*|&$(RAJA_LINK_DEPENDS) |' ../Makefile.package
    #~ sed -i -e 's|^PKG_SYSINC =[ \t]*|&$(RAJA_CPPFLAGS) $(RAJA_CXXFLAGS) |' ../Makefile.package
    #~ sed -i -e 's|^PKG_SYSLIB =[ \t]*|&$(RAJA_LDFLAGS) |' ../Makefile.package
#~ #    sed -i -e 's|^PKG_SYSPATH =[ \t]*|&$(kokkos_SYSPATH) |' ../Makefile.package
  #~ fi

  #~ if (test -e ../Makefile.package.settings) then
    #~ sed -i -e '/CXX\ =\ \$(CC)/d' ../Makefile.package.settings
    #~ sed -i -e '/^include.*raja.*$/d' ../Makefile.package.settings
    #~ # multiline form needed for BSD sed on Macs
    #~ sed -i -e '4 i \
#~ CXX = $(CC)
#~ ' ../Makefile.package.settings
    #~ sed -i -e '5 i \
#~ include ..\/..\/lib\/RAJA\/build\/Makefile
#~ ' ../Makefile.package.settings
  #~ fi

  #~ #  comb/omp triggers a persistent bug in nvcc. deleting it.
  #~ rm -f ../*_comb_omp.*

#~ elif (test $1 = 2) then

  #~ #  comb/omp triggers a persistent bug in nvcc. deleting it.
  #~ rm -f ../*_comb_omp.*

#~ elif (test $1 = 0) then

  #~ if (test -e ../Makefile.package) then
    #~ sed -i -e 's/[^ \t]*raja[^ \t]* //g' ../Makefile.package
    #~ sed -i -e 's/[^ \t]*RAJA[^ \t]* //g' ../Makefile.package
  #~ fi

  #~ if (test -e ../Makefile.package.settings) then
    #~ sed -i -e '/CXX\ =\ \$(CC)/d' ../Makefile.package.settings
    #~ sed -i -e '/^include.*raja.*$/d' ../Makefile.package.settings
  #~ fi

#~ fi
echo "RAJA Package Added Successfully!"
