/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef LMP_ACCELERATOR_RAJA_H
#define LMP_ACCELERATOR_RAJA_H

// true interface to RAJA
// used when RAJA is installed

//~ #ifdef LMP_RAJA_DUMMY
// #ifdef LMP_RAJA

//~ #include "raja.h"
//#include "atom_raja.h"
//#include "comm_raja.h"
//#include "comm_tiled_raja.h"
//#include "domain_raja.h"
//#include "neighbor_raja.h"
// #include "memory_raja.h"
//#include "modify_raja.h"

//~ #define LAMMPS_INLINE RAJA_INLINE_FUNCTION

//~ #else

// dummy interface to RAJA
// needed for compiling when RAJA is not installed

//~ #include "atom.h"
//~ #include "comm_brick.h"
//~ #include "comm_tiled.h"
//~ #include "domain.h"
//~ #include "neighbor.h"
//~ #include "memory.h"
//~ #include "modify.h"

//~ #define LAMMPS_INLINE inline

//~ namespace LAMMPS_NS {

//~ class RajaLMP {
 //~ public:
  //~ int raja_exists;
  //~ int num_threads;
  //~ int ngpu;
  //~ int numa;

  //~ RajaLMP(class LAMMPS *, int, char **) {raja_exists = 0;}
  //~ ~RajaLMP() {}
  //~ void accelerator(int, char **) {}
  //~ int neigh_list_raja(int) {return 0;}
  //~ int neigh_count(int) {return 0;}
//~ };



// class AtomRaja : public Atom {
//  public:
//   tagint **k_special;
//   AtomRaja(class LAMMPS *lmp) : Atom(lmp) {}
//   ~AtomRaja() {}
//   void sync(const ExecutionSpace , unsigned int ) {}
//   void modified(const ExecutionSpace , unsigned int ) {}
// };

// class CommRaja : public CommBrick {
//  public:
//   CommRaja(class LAMMPS *lmp) : CommBrick(lmp) {}
//   ~CommRaja() {}
// };

// class CommTiledRaja : public CommTiled {
//  public:
//   CommTiledRaja(class LAMMPS *lmp) : CommTiled(lmp) {}
//   CommTiledRaja(class LAMMPS *lmp, Comm *oldcomm) : CommTiled(lmp,oldcomm) {}
//   ~CommTiledRaja() {}
// };

// class DomainRaja : public Domain {
//  public:
//   DomainRaja(class LAMMPS *lmp) : Domain(lmp) {}
//   ~DomainRaja() {}
// };

// class NeighborRaja : public Neighbor {
//  public:
//   NeighborRaja(class LAMMPS *lmp) : Neighbor(lmp) {}
//   ~NeighborRaja() {}
// };

// class MemoryRaja : public Memory {
//  public:
//   MemoryRaja(class LAMMPS *lmp) : Memory(lmp) {}
//   ~MemoryRaja() {}
//   void grow_raja(tagint **, tagint **, int, int, const char*) {}
// };

// class ModifyRaja : public Modify {
//  public:
//   ModifyRaja(class LAMMPS *lmp) : Modify(lmp) {}
//   ~ModifyRaja() {}
// };

// class DATRaja {
//  public:
//   typedef double tdual_xfloat_1d;
//   typedef double tdual_FFT_SCALAR_1d;
//   typedef int tdual_int_1d;
//   typedef int tdual_int_2d;
// };

/* NOT IMPLEMENTED YET

 --------------------------- */

//~ }

//~ #endif
#endif
