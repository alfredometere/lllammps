#ifndef LMP_GIT_VERSION_H
#define LMP_GIT_VERSION_H
const bool LAMMPS_NS::LAMMPS::has_git_info = true;
const char LAMMPS_NS::LAMMPS::git_commit[] = "e1295c819b6fac6ab5a76fc6c42a210a86e19374";
const char LAMMPS_NS::LAMMPS::git_branch[] = "master";
const char LAMMPS_NS::LAMMPS::git_descriptor[] = "";
#endif
