#ifndef LMP_GIT_VERSION_H
#define LMP_GIT_VERSION_H
const bool LAMMPS_NS::LAMMPS::has_git_info = true;
const char LAMMPS_NS::LAMMPS::git_commit[] = "0220f00732e31916ae38495bfa7bc6415107e265";
const char LAMMPS_NS::LAMMPS::git_branch[] = "master";
const char LAMMPS_NS::LAMMPS::git_descriptor[] = "";
#endif

